<?php

use Illuminate\Database\Seeder;
use App\Genre;
use Tmdb\Laravel\Facades\Tmdb;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $requests_counter = 0;
      $tmdb_genres = Tmdb::getGenresApi()->getMovieGenres();
      foreach ($tmdb_genres['genres'] as $tmdb_genre)
      {
        $tmdb_genre = Genre::firstOrCreate(array(
                                                  'name'=>$tmdb_genre['name'],
                                                  'tmdb_id'=>$tmdb_genre['id']
                                          ));
      }
    }
}
