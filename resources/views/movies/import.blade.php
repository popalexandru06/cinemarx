@extends('layouts.application')

@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Import date
        </h1>
    </div>
@endsection

@section('content')
    <div class="row">
        @if($movies['total_results'] > 0)
            <div class="alert alert-info">
                <ul>
                    <li>
                        S-au găsit {{$movies['total_results']}} filme aparute 
                        in perioada {{$search_params['primary_release_date.gte']}} si {{$search_params['primary_release_date.lte']}}    
                        .
                    </li>
                    <li>
                        Au fost importate {{count($movies['results'])}} de filme de pe pagina {{$search_params['page']}} din {{$movies['total_pages']}} pagini.
                        @if ($search_params['page'] < $movies['total_pages'])
                            <form action="{{ route('import') }}" method="POST">
                                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                                <input type="hidden" name="search[page]" value="{{((int)$search_params['page'])+1}}" />
                                <input type="hidden" name="search[primary_release_date.gte]" value="{{$search_params['primary_release_date.gte']}}">
                                <input type="hidden" name="search[primary_release_date.lte]" value="{{$search_params['primary_release_date.lte']}}">
                                <input type="submit" class="btn btn-primary" value="Importă filme de pe pagina urmatoare">
                            </form>
                        @endif
                    </li>

                </ul>
            </div>
        @else
        @endif
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-condensed table-striped">
                <thead>
                    <tr>
                        <th>Titlu</th>
                        <th>Data lansarii</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($movies['results'] as $movie)
                        <tr>
                            <td>{{$movie['title']}}</td>
                            <td>{{$movie['release_date']}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection