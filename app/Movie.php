<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
  protected $fillable = ['title', 'release_date', 'tmdb_id'];

  public function genres() {
      return $this->belongsToMany('App\Genre'); 
  }
}
