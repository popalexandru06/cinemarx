<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Movie;
use App\Genre;
use Illuminate\Http\Request;
use Tmdb\Laravel\Facades\Tmdb;
use Session;

class MovieController extends Controller {

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $movies = Movie::orderBy('release_date', 'desc')->paginate(10);
    Session::put('tmdb_requests_counter', 0);

    return view('movies.index', compact('movies'));
  }

  public function import(Request $request)
  {
    $movies = Tmdb::getDiscoverApi()->discoverMovies($request->search);
    Session::put('tmdb_requests_counter', Session::get('tmdb_requests_counter') + 1);
    
    foreach ($movies['results'] as $tmdb_movie) 
    {
      $movie = Movie::firstOrCreate(array('title'=>$tmdb_movie['title'],
                                          'tmdb_id'=>$tmdb_movie['id'],
                                          'release_date'=>$tmdb_movie['release_date']
                                    ));
      // Check if movie is created now and it needs to attach genres to it
      if($movie->wasRecentlyCreated){
        foreach ($tmdb_movie['genre_ids'] as $key => $genre_id)
        {
          $genre = Genre::where('tmdb_id', '=' ,$genre_id)->first();
          if($genre == null){
            $tmdb_genre = Tmdb::getGenresApi()->getGenre($genre_id);
            $tmdb_genre_name = $tmdb_genre_name['name'];
            Session::put('tmdb_requests_counter', Session::get('tmdb_requests_counter') + 1);
            $genre = Genre::firstOrCreate(array('name'=>$tmdb_genre_name,
                                             'tmdb_id'=>$genre_id
                                         ));
          }
          $movie->genres()->attach($genre->id);
        }
      }

    }
    
    if ($request->search['page'] < $movies['total_pages']){
      return view('movies.import', ['movies' => $movies,
                                    'search_params'  => $request->search
                                   ]);  
    }else{
      return redirect()->route('root')->with('message', 'Importul s-a finalizat cu success. Au fost importate '.$movies["total_results"].' filme. Si s-au facut '.Session::get('tmdb_requests_counter').' request-uri către TMDB');
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */

  public function show($id)
  {
    $movie = Movie::findOrFail($id);

    return view('movies.show', compact('movie'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */

}