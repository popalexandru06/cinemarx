@extends('layouts.application')

@section('header')
    <div class="page-header clearfix">
        <div class="col-md-9">
            <h1>
                <i class="glyphicon glyphicon-align-justify"></i> Ultimele Filme
            </h1>
        </div>
        <div class="col-md-3 import-button">
            <form action="{{ route('import') }}" method="POST">
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                <input type="hidden" name="search[page]" value="1" />
                <input type="hidden" name="search[primary_release_date.gte]" value="{{ date('Y-m-d') }}">
                <input type="hidden" name="search[primary_release_date.lte]" value="{{ date('Y-m-d') }}">
                <div class="col-md-4">
                    <input type="submit" class="btn btn-primary" value="Importă filmele aparute azi">
                </div>
            </form>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        @if(session('message'))
            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {{session('message')}}</em></div>
        @endif
    </div>
    <div class="row">
        <form action="{{ route('import') }}" method="POST">
            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
            <input type="hidden" name="search[page]" value="1" />
            <div class="col-md-4">
                <input type="text" name="search[primary_release_date.gte]" value="" class="form-control datepicker" id="start_date">
            </div>
            <div class="col-md-4">
                <input type="text" name="search[primary_release_date.lte]" value="" class="form-control datepicker" id="end_date">
            </div>
            <div class="col-md-4">
                <input type="submit" class="btn btn-info" value="Importă filmele din perioada selectată">
            </div>
        </form>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if($movies->count())
                <hr>
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>Titlu</th>
                            <th>Data lansarii</th>
                            <th>Genul filmului</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($movies as $movie)
                            <tr>
                                <td>{{$movie->title}}</td>
                                <td>{{$movie->release_date}}</td>
                                <td>
                                    @foreach($movie->genres as $genre)
                                        {{$genre->name}}
                                        @unless($loop->last)
                                        ,
                                        @endunless
                                    @endforeach
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $movies->render() !!}
            @else
                <h3 class="text-center alert alert-info">Niciun rezultat!</h3>
            @endif 
        </div>
    </div>

@endsection